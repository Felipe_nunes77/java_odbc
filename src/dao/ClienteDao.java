package dao;
import java.awt.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Cliente;

public class ClienteDao{
    private DataSource dataSource;
    
    public ClienteDao(DataSource dataSource ){
        this.dataSource = dataSource;
    }
    public ArrayList<Cliente>readAll(){
        try {
            String SQL = "select * from cliente";
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();
            ArrayList<Cliente> lista = new ArrayList<Cliente>();
            while(rs.next()){
                Cliente cli =new Cliente();
                cli.setId(rs.getInt("id"));
                cli.setNome(rs.getString("nome"));
                cli.setEmail(rs.getString("email"));
                cli.setTelefone(rs.getString("telefone"));
                lista.add(cli);
            }           
            ps.close();
            return lista;            
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }
    
    public void InsertCliente(Cliente c){
         String SQL = "insert into cliente (id,nome, email, telefone) values(";        
            SQL= SQL +c.getId()+",'";
            SQL= SQL +c.getNome()+"','";
            SQL= SQL +c.getEmail()+"','";
            SQL= SQL +c.getTelefone()+"')";
           
         try{
            PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
             ps.execute();
         }catch(SQLException e) {
            System.err.println(e.getMessage());
             
         }
    }
}
