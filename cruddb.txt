﻿-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11-Out-2017 às 03:00
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cruddb`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `email`, `telefone`) VALUES
(1, 'ZAZA', 'zaza@teste.com', '22222'),
(2, 'Jair Bosonaro', 'jbosonaro@teste.com', '115415545'),
(3, 'Maze', 'maze@teste.com', '115415545'),
(4, 'Baleia Gorda', 'bgorda@teste.com', '115415545'),
(5, 'paulo', 'teste@teste', '46'),
(6, 'asdfsa', 'fsdfas', '54'),
(7, 'fasdf', 'fdsfasf', 'rfsefsf'),
(8, 'fasdf', 'fdsfasf', 'rfsefsf'),
(10, 'janailson', 'jan@teste', '119552222');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;




Lista de comandos GIT.txt
Type
Text
Size
2 KB (2,448 bytes)
Storage used
0 bytesOwned by someone else
Location
Temporario
Owner
Daniel Ferreira De Barros Jr
Modified
7:43 PM by Daniel Ferreira De Barros Jr
Opened
8:06 PM by me
Created
8:15 AM
No description
Viewers can download

1 - Criar conta no BitBucket: https://bitbucket.org/product

2 - Abrir o GIT BASH
	Crie uma pasta no diretório do aluno: exemplo: e:\Java

	digite no Git BASH:
	cd e:
	cd Java/
	
3 - Configurar o proxy no git:
	COMANDO PADRÃO (não executar este):
	git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:8080

	EXEMPLO: (devem ser executado os DOIS comandos)
	git config --global http.proxy http://RA:SenhaDoAluno@186.251.39.91:3128
	git config --global https.proxy https://RA:SenhaDoAluno@186.251.39.91:3128

	Para ler o proxy configurado:
	git config --global --get http.proxy
	git config --global --get https.proxy

	Comando para LIMPAR as configurações do proxy (no final da aula):
	git config --global --unset http.proxy
	git config --global --unset https.proxy

4 - Link com comandos de apoio para GIT
	http://rogerdudler.github.io/git-guide/index.pt_BR.html

5 - Configurar os seus dados no git:
	git config --global user.email "you@example.com"
  	git config --global user.name "Your Name"

6 - Clonar repositorio remoto:
	git clone https://prodanielf@bitbucket.org/prodanielf/sa_3m.git

7 - Atualizar repositório (dentro do diretório versionado):
	git pull origin master

-----------------------------------------------------------------------------------

8 - Para criar um novo repositorio local:
	// para iniciar o repositório
	git init

	// para adicionar o versionamento aos arquivos criados ou  modificados
	git add *

	// para listar os arquivos (se estão versionados ou não)
	git status
	
	// para determinar o repositorio remoto (destino dos arquivos)
	git remote add origin <servidor>

	// para efetivar as alterações no repositório local
	git commit -m "comentário_exemplo"

	// enviar os arquivos commitados para o repositório remoto
	git push origin master


9 - Preencher o formulário da sua conta (git ou bitbucket)
	goo.gl/uQSiG2

	Agora você deve conferir se os dados estão no servidor remoto (Bitbucket)


10 - Estudo extra!! (veja tudo)
	http://rogerdudler.github.io/git-guide/index.pt_BR.html
	https://pt.wikipedia.org/wiki/Sistema_de_controle_de_vers%C3%B5es
	https://www.youtube.com/watch?v=C18qzn7j4SM&list=PLQCmSnNFVYnRdgxOC_ufH58NxlmM6VYd1
	https://www.youtube.com/watch?v=WVLhm1AMeYE&list=PLInBAd9OZCzzHBJjLFZzRl6DgUmOeG3H0
	https://www.youtube.com/watch?v=UMhskLXJuq4	
	
     COMANDOS EXTRAS:
	ls = listar diretório
	
